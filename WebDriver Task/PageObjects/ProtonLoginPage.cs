﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using OpenQA.Selenium.Support;
using SeleniumExtras.PageObjects;
using NUnit.Framework;

namespace WebDriver_Task.PageObjects
{
    internal class ProtonLoginPage
    {
        private readonly IWebDriver _driver;
        private readonly string _url = @"https://account.proton.me/login";

        public ProtonLoginPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "username")]
        public IWebElement UserNameInput
        {
            get;
            set;
        }

        [FindsBy(How = How.Id, Using = "password")]
        public IWebElement PasswordInput
        {
            get;
            set;
        }

        [FindsBy(How = How.XPath, Using = "//button[text()='Sign in']")]
        public IWebElement SignInButton
        {
            get;
            set;
        }

        public ProtonLoginPage Navigate()
        {
            _driver.Navigate().GoToUrl(_url);
            return this;
        }

        public ProtonMailboxPage LogIn(string userName, string password)
        {
            UserNameInput.Clear();
            UserNameInput.SendKeys(userName);

            PasswordInput.Clear();
            PasswordInput.SendKeys(password);

            SignInButton.Click();

            return new ProtonMailboxPage(_driver);
        }
    }
}
