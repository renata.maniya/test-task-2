﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace WebDriver_Task.PageObjects
{
    internal class ProtonMailboxPage
    {
        private readonly IWebDriver _driver;
        private readonly string _url = @"https://mail.proton.me/u/4/inbox";

        public ProtonMailboxPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//div[contains(@class, 'unread')]")]
        public IWebElement email
        {
            get;
            set;
        }

        public void Navigate()
        {
            _driver.Navigate().GoToUrl(_url);
        }

        public bool IsEmailUnread()
        {
            return email != null;
        }

        public bool IsCorrectSender()
        {
            IWebElement email = _driver.FindElement(By.XPath("//span[@title='firsttestfakeaccount@yahoo.com']"));
            return email != null;
        }

        public string ReadEmailContent()
        {
            IWebElement email = _driver.FindElement(By.XPath("//*[@data-element-id='Gyy9UySSA74yJu81sdrsyHChJXzyTC6EMcadYq2Y0MZn4mhFfqMGDgpGwjRS-3BNnXKFvjUJdNXm914wgzq83g==']"));
            return email.Text;
        }





    }
}
