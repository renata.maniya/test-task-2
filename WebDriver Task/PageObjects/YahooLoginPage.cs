﻿using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace WebDriver_Task.PageObjects
{
    internal class YahooLoginPage
    {
        private readonly IWebDriver _driver;
        private readonly string url = @"https://login.yahoo.com/?.intl=en&.lang=en-US&src=ym&activity=header-mail&pspid=2114714002done=https%3A%2F%2Fmail.yahoo.com%2%2FdFfolders%2F1&add=1";

        public YahooLoginPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "login-username")]
        public IWebElement UserNameInput
        {
            get;
            set;
        }

        [FindsBy(How = How.Id, Using = "login-passwd")]
        public IWebElement PasswordInput
        {
            get;
            set;
        }

        [FindsBy(How = How.Id, Using = "login-signin")]
        public IWebElement LoginButton
        {
            get;
            set;
        }

        public YahooLoginPage Navigate()
        {
            _driver.Navigate().GoToUrl(url);
            return this;
        }

        public YahooMailboxPage LogIn(string userName, string password)
        {
            UserNameInput.Clear();
            UserNameInput.SendKeys(userName);

            LoginButton.Click();

            Thread.Sleep(200);

            PasswordInput.Clear();
            PasswordInput.SendKeys(password);

            LoginButton.Click();

            return new YahooMailboxPage(_driver);
        }

        public YahooLoginPage SetUserName(string userName)
        {
            UserNameInput.Clear();
            UserNameInput.SendKeys(userName);
            return this;
        }

        public YahooLoginPage SetPassword(string password)
        {
            PasswordInput.Clear();
            PasswordInput.SendKeys(password);
            return this;
        }

        public YahooLoginPage ClickLoginButton()
        {
            LoginButton.Click();
            return this;
        }

        public IWebElement GetUserNameErrorMessage()
        {
            return _driver.FindElement(By.XPath("//p[@id='username-error']"));          
        }

        public IWebElement GetPasswordErrorMessage()
        {
            return _driver.FindElement(By.XPath("//form[@class='challenge-form ']/p[@class='error-msg']"));
        }
    }
}
