﻿using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace WebDriver_Task.PageObjects
{
    internal class YahooMailboxPage
    {
        private readonly IWebDriver _driver;
        private readonly string _url = @"https://mail.yahoo.com/d/folders/1";

        public YahooMailboxPage(IWebDriver driver)
        {
            _driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//a[@data-test-id='compose-button']")]
        public IWebElement ComposeButton
        {
            get;
            set;
        }

        [FindsBy(How = How.XPath, Using = "//a[@data-test-id='compose-button']")]
        public IWebElement RecipientInput
        {
            get;
            set;
        }

        [FindsBy(How = How.XPath, Using = "//input[@data-test-id='compose-subject']")]
        public IWebElement SubjectInput
        {
            get;
            set;
        }

        [FindsBy(How = How.XPath, Using = "//*[@id=\"editor-container\"]/div[1]")]
        public IWebElement BodyInput
        {
            get;
            set;
        }

        [FindsBy(How = How.XPath, Using = "//button[@data-test-id='compose-send-button']")]
        public IWebElement SendButton
        {
            get;
            set;
        }

        public void Navigate()
        {
            _driver.Navigate().GoToUrl(_url);
        }

        public void ClickComposeButton()
        {
            ComposeButton.Click();
        }

        public void EnterRecipient(string recipientName)
        {
            RecipientInput.Clear();
            RecipientInput.SendKeys(recipientName);
        }

        public void EnterSubject(string subjectText)
        {
            SubjectInput.Clear();
            SubjectInput.SendKeys(subjectText);
        }

        public void EnterBody(string bodyText) 
        {
            BodyInput.Clear();
            BodyInput.SendKeys(bodyText);
        }

        public void ClickSendButton()
        {
            SendButton.Click();
        }
    }
}
