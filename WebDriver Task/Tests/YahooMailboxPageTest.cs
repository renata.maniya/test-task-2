﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using WebDriver_Task.PageObjects;

namespace WebDriver_Task.Tests
{
    internal class YahooMailboxPageTest
    {
        private IWebDriver _driver;
        private YahooMailboxPage _mailboxPage;

        [SetUp]
        public void SetupTest()
        {
            _driver = new ChromeDriver();
            _mailboxPage = new YahooMailboxPage(_driver);
        }

        [Test]
        public void SendMessage()
        {
            //Arrange
            var recipientName = "secondtestfakeaccount@proton.me";
            var subjectText = "Test message";
            var bodyText = "Hello! This is a test message.";

            //Act
            _mailboxPage.Navigate();
            _mailboxPage.ClickComposeButton();
            _mailboxPage.EnterRecipient(recipientName);
            _mailboxPage.EnterSubject(subjectText);
            _mailboxPage.EnterBody(bodyText);
            _mailboxPage.ClickSendButton();
        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Quit();
        }
    }
}
