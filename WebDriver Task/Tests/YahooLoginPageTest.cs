﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using WebDriver_Task.PageObjects;

namespace WebDriver_Task.Tests
{    
    internal class YahooLoginPageTest
    {
        private IWebDriver _driver;
        private YahooLoginPage _loginPage;

        [SetUp]
        public void SetupTest()
        {
            _driver = new ChromeDriver();
            _loginPage = new YahooLoginPage(_driver);
        }

        [Test]
        public void TestSeccessfulLogin() 
        {
            //Arrange
            string userName = "firsttestfakeaccount";
            string password = "Correct_Password_1";

            //Act
            _loginPage
                .Navigate()
                .LogIn(userName, password);

            var urlParamsIndex = _driver.Url.IndexOf('?');
            var actualUrl = _driver.Url[..urlParamsIndex];

            //Assert
            Assert.AreEqual("https://mail.yahoo.com/d/folders/1", actualUrl);
        }

        [Test]
        public void TestLoginWithEmptyUserName()
        {
            //Arrange
            string userName = string.Empty;

            //Act
            _loginPage
                .Navigate()
                .SetUserName(userName)
                .ClickLoginButton();

            Thread.Sleep(200);

            //Assert
            IWebElement errorMessage = _loginPage.GetUserNameErrorMessage();
            Assert.IsTrue(errorMessage.Displayed, "Error message should be displayed for empty user name.");
        }

        [Test]
        public void TestLoginWithIncorrectUsername()
        {
            //Arrange
            string userName = "firstaccountNotExistingNotExistingNotExisting";

            //Act
            _loginPage
                .Navigate()
                .SetUserName(userName)
                .ClickLoginButton();

            Thread.Sleep(200);

            //Assert
            IWebElement errorMessage = _loginPage.GetUserNameErrorMessage();
            Assert.IsTrue(errorMessage.Displayed, "Error message should be displayed for user name.");
        }

        [Test]
        public void TestLoginWithEmptyPassword()
        {
            //Arrange
            string userName = "firsttestfakeaccount";
            string password = string.Empty;

            //Act
            _loginPage
                .Navigate()
                .SetUserName(userName)
                .ClickLoginButton();

            Thread.Sleep(200);

            _loginPage
                .SetPassword(password)
                .ClickLoginButton();

            Thread.Sleep(200);

            //Assert
            IWebElement errorMessage = _loginPage.GetPasswordErrorMessage();
            Assert.IsTrue(errorMessage.Displayed, "Error message should be displayed for empty password.");
        }

        [Test]
        public void TestLoginWithIncorrectPassword()
        {
            //Arrange
            string userName = "firsttestfakeaccount";
            string password = "Incorrect_Password_1";

            //Act
            _loginPage
                .Navigate()
                .SetUserName(userName)
                .ClickLoginButton();

            Thread.Sleep(200);

            _loginPage
                .SetPassword(password)
                .ClickLoginButton();

            Thread.Sleep(200);

            //Assert
            IWebElement errorMessage = _loginPage.GetPasswordErrorMessage();
            Assert.IsTrue(errorMessage.Displayed, "Error message should be displayed for incorrect password.");
        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Quit();
        }
    }
}
