﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using WebDriver_Task.PageObjects;


namespace WebDriver_Task.Tests
{
    internal class ProtonLoginPageTest
    {
        private IWebDriver _driver;
        private ProtonLoginPage _loginPage;

        [SetUp]
        public void SetupTest()
        {
            _driver = new ChromeDriver();
            _loginPage = new ProtonLoginPage(_driver);
        }

        [Test]

        public void TestSeccessfulLogin() 
        {
            //Arrange
            var userName = "secondtestfakeaccount";
            var password = "Correct_Password_2";

            //Act
            _loginPage
                .Navigate()
                .LogIn(userName, password);

            //Assert
            Assert.AreEqual("https://mail.proton.me/u/4/inbox", _driver.Url);
        }

        [TearDown]
        public void Cleanup()
        {
            _driver.Quit();
        }
    }
}
